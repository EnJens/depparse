from __future__ import print_function, unicode_literals
from elftools.elf.elffile import ELFFile
import os.path


def main(args):
    all_symbols = set()
    all_deps = set()
    all_provided_syms = set()
    missing_deps = []
    files = args.files
    while files:
        file = files[0]
        files = files[1:]
        try:
            symbols, provided_syms, deps = parse_elf(file, args.libdir)
        except IOError:
            missing_deps.append(file)
            continue

        for file in deps:
            if file not in all_deps:
                files.append(file)

        all_symbols = all_symbols.union(symbols)
        all_deps = all_deps.union(deps)
        all_provided_syms = all_provided_syms.union(provided_syms)
        all_deps = all_deps.difference(missing_deps)

    print("Deps: %r" % all_deps)
    print("Missing Syms: %r" % (all_symbols.difference(all_provided_syms)))
    print("Missing Deps: %r" % missing_deps)


def find_so(filename, libdirs):
    if os.path.isfile(filename):
        return filename

    for dir in libdirs:
        fullpath = os.path.join(dir, filename)
        if os.path.isfile(fullpath):
            return fullpath
    return None


def parse_elf(filename, libdirs):
    dependencies = []
    symbols = []
    undef_syms = []
    filename = find_so(filename, libdirs)
    if not filename:
        raise IOError("File not found")

    with open(filename, 'rb') as f:
        elf = ELFFile(f)
        reldyn = elf.get_section_by_name(b'.dynamic')

        for tag in reldyn.iter_tags('DT_NEEDED'):
            dependencies.append(tag.needed)
        relsym = elf.get_section_by_name(b'.dynsym')
        for sym in relsym.iter_symbols():
            if not sym.name:
                continue
            if sym['st_info']['type'] != 'STT_FUNC':
                continue
            if sym['st_shndx'] == 'SHN_UNDEF':
                undef_syms.append(sym.name)
            else:
                symbols.append(sym.name)

    return set(undef_syms), set(symbols), set(dependencies)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--libdir', action='append', required=True)
    parser.add_argument('files', metavar='library file', nargs='+')
    args = parser.parse_args()
    main(args)
